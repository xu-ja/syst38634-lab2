package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import phones.*;

public class TestSmartPhone {
	@Test
	public void testGetFormattedPrice() {
		SmartPhone phone = new SmartPhone("Phone", 1000.00, 5.00);
		assertEquals("invalid format", "$1,000", phone.getFormattedPrice());
	}
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		SmartPhone phone = new SmartPhone("iPhone", 999.0, 1.00);
		assertEquals("invalid format", "$999", phone.getFormattedPrice());
	}
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone phone = new SmartPhone("iPhone", 999.1, 1.00);
		assertEquals("invalid format", "$999.1", phone.getFormattedPrice());
	}
	@Test(expected = NullPointerException.class) 
	public void testGetFormatedPriceException() { 
		SmartPhone phone = null;
		phone.getFormattedPrice();
	}
	
	@Test(expected = VersionNumberException.class)
	public void testSetVersion() throws VersionNumberException {
		SmartPhone phone = new SmartPhone("phone", 1000, 1);
		phone.setVersion(5);
	}
	@Test(expected = VersionNumberException.class)
	public void testSetVersionBoundaryIn() throws VersionNumberException {
		SmartPhone phone = new SmartPhone("phone", 1000, 1);
		phone.setVersion(4.01);
	}
	@Test()
	public void testSetVersionBoundaryOut() throws VersionNumberException {
		try {
			SmartPhone phone = new SmartPhone("phone", 1000, 1);
			phone.setVersion(3.99);
		} catch (VersionNumberException e) {
			fail("Should not throw exception");
		}
		
	}
	@Test(expected = NullPointerException.class)
	public void testSetVersionNumberException() throws VersionNumberException {
		SmartPhone phone = null;
		phone.setVersion(5);
	}

}
